<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<title>Form</title>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body style="color:#ff6c36">
        <div class="Form">

          <p><a href="admin.php">Админка</a></p>

        <div id="events">
                <p>Заполните эту форму иначе кусь</p>
            </div>
            <form method="post" action="index.php" name="contract" >
                <?php
                unset($_SERVER['PHP_AUTH_USER']);
                unset($_SERVER['PHP_AUTH_PW']);
                if(empty($_SESSION['login'])){
                    print('Вход по логину и паролю <a href="login.php">здесь</a> .</br>');
                }
                if(!empty($messages['login_and_password'])){
                    print($messages['login_and_password']);
                }
                if (!empty($messages['save'])) {
                    print($messages['save']);
                }
                ?>
                <div id="nam">
                    <?php
                    $ERROR='';
                    $name='';
                    if (!empty($messages['name'])) {
                        print($messages['name']);
                        $ERROR='error';
                    }
                    if(!empty($values['name'])){
                        $name=$values['name'];
                    }
                    ?>
				<div id="nam">
					<p>Name:<input maxlength="25" size="40" name="name" placeholder="Введите имя" class="<?php print $ERROR?>" value="<?php print $name?>"></p>
				</div>
				<div id="address">
				<?php
                    $ERROR='';
                    $email='';
                    if (!empty($messages['email'])) {
                        print($messages['email']);
                        $ERROR='error';
                    }
                    if(!empty($values['email'])){
                        $email=$values['email'];
                    }
                    ?>
					<p>Email:<input name="email" value="<?php print $email?>" class="<?php print $ERROR?>" placeholder="vivivivi@index.com"></p>
				</div>
				<br>
				<div id="BIRHYEAR">
				<?php
                    $ERROR='';
                    if (!empty($messages['year'])) {
                        print($messages['year']);
                        $ERROR='error';
                    } 
					 if(!empty($values['year'])){
                        $year=$values['year'];
                    }?>
				Year of Birth:
				<span class="<?php print $ERROR?>">
                        <select name="year" size="1">
                            <?php
                            $select=array(1517=>'',1518=>'',1519=>'',1520=>'',1521=>'',1522=>'');
                            for($s=1517;$s<=1522;$s++){
                                if($values['year']==$s){
                                    $select[$s]='selected';break;
                                }
                            }
                            ?>
						<option value="1517" <?php print $select[1517]?>>1517</option>
						<option value="1518" <?php print $select[1518]?>>1518</option>
						<option value="1519" <?php print $select[1519]?>>1519</option>
						<option value="1520" <?php print $select[1520]?>>1520</option>
						<option value="1521" <?php print $select[1521]?>>1521</option>
						<option value="1522" <?php print $select[1522]?>>1522</option>
					</select>	
					</span>
				</div>
				</br>
				<div id="SEX">
                    <?php
                    $ERROR='';
                    $sex='';
                    if (!empty($messages['sex'])) {
                        print($messages['sex']);
                        $ERROR='error';
                    }
                    if(!empty($values['sex'])){
                        $sex=$values['sex'];
                    }
                    ?>
                Пол:    <span class="<?php print $ERROR?>">
                            <input type="radio" value="M" name="sex"<?php if($values['sex']=='M') {print'checked';}?> >Man
                            <input type="radio" value="F" name="sex"<?php if($values['sex']=='F') {print'checked';}?> >Female
                    </span>
                </div>
                </br>
				<div id="LIMBS">
                    <?php
                    $ERROR='';
                    if (!empty($messages['limbs'])) {
                        print($messages['limbs']);
                        $ERROR='error';
                    }
                    ?>
                    Конечности:<?php
                    $select_limbs=array(32=>'',64=>'',128=>'',256=>'',512=>'');
                    for($s=32;$s<=512;$s++){
                        if($values['limbs']==$s){
                            $select_limbs[$s]='checked';break;
                        }
                    }
                    ?>
                    <span class="<?php print $ERROR?>">
                        <input type="radio" value="32" name="limbs" <?php print $select_limbs[32]?>>32
                        <input type="radio" value="64" name="limbs" <?php print $select_limbs[64]?>>64
                        <input type="radio" value="128" name="limbs" <?php print $select_limbs[128]?>>128
                        <input type="radio" value="256" name="limbs" <?php print $select_limbs[256]?>>256
                        <input type="radio" value="512" name="limbs" <?php print $select_limbs[512]?>>512
                </span>
				</div>
                </br>
		
		
		
		<div id="SUPERPOWERS" >
                    <?php
                    $ERROR='';
                    if(!empty($messages['sverh'])){
                        print($messages['sverh']);
                        $ERROR='error';
                    }?>
                    <span >
                        Суперспособности:</br>
                        <?php
                         if(!empty($values['sverh'])){
                             $flag=FALSE;
                             $SVERH_PROVERKA = array("net" =>"", "GodMode" =>"", "Rage" =>"", "Infinity ammo" =>"", "Pyrokinesis" =>"");
                             $SVERH = unserialize($values['sverh']);
                            if(!empty($SVERH))foreach ($SVERH as $E){
                                if($E=="net"){
                                    $SVERH_PROVERKA["net"]="selected";
                                $flag=TRUE;break;}
                            }
                            if(!empty($SVERH))
                                    if(!$flag){
                                        foreach ($SVERH as $T){
                                            $SVERH_PROVERKA["$T"]="selected";
                                        }
                                    }
                         }
                        ?>
					   <select id="sposobnost" name="sverh[]" multiple="multiple" size="3" class="<?php print $ERROR?>">
                            <option value="net" <?php if(!empty($values['sverh'])) print $SVERH_PROVERKA["net"]?>>None</option>
                            <option value="GodMode"<?php if(!empty($values['sverh'])) print $SVERH_PROVERKA["GodMode"]?> >Бессмертие</option>
                            <option value="Rage"<?php if(!empty($values['sverh'])) print $SVERH_PROVERKA["Rage"]?> >Ярость</option>
                            <option value="Infinity ammo"<?php if(!empty($values['sverh'])) print $SVERH_PROVERKA["Infinity ammo"]?> >Бесконечные патроны</option>
                            <option value="Pyrokinesis"<?php if(!empty($values['sverh'])) print $SVERH_PROVERKA["Pyrokinesis"]?> >Пирокинез</option>
                        </select>
                    </span>
                </div>
                </br>
				
				    <div id="biography">
                        <?php
                        $ERROR='';
                        $BIO='';
                        if (!empty($messages['biography'])) {
                            print($messages['biography']);
                            $ERROR='error';
                        }
                        if(!empty($values['biography'])){
                            $BIO=$values['biography'];
                        }
                        ?>
                        <p class="<?php print $ERROR?>" >
                            Ваша биография: </br>
                            <textarea cols="45" name="biography" placeholder="Пишите правду, толкьо правду и ничего кроме правды"><?php if($values['biography']){print $values['biography'];} ?></textarea>
                        </p>
                    </div>
                </br>
            <div id="Consent"  >
                    <?php
                    $ERROR='';
                    $consent='';
                    if (!empty($messages['consent'])) {
                        print($messages['consent']);
                        $ERROR='error';
                    }
                    if(!empty($values['consent'])){
                        $consent='checked';
                    }
                    ?>
                    <span class="<?php print $ERROR?>">Вы согласны?
			<input type="checkbox" name="consent"  value="yes" <?php print $consent?>>
            </span>
                </div>
                </br>
				<input type="submit" value="Отправить">
			</form>
            <?php   
                    if(!empty($_SESSION['login'])){
                        print('<form method="POST" action="login.php"><input type="submit" name="exit" value="Выход"></form>');
                    }
                ?>
			</form>
		</div>	
	</body>
</html>